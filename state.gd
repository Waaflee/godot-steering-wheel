extends Node

var state := {
	"connected": false,
	"connecting": false,
	"ip": ""
	}

var command := {
	"up": false,
	"down": false,
	"left": false,
	"left_degree": 0.0,
	"right": false,
	"right_degree": 0.0,
	} 

# Automatic Server IP Detection
const UDP_BROADCAST_FREQUENCY: float = 3.0
var udp_network: PacketPeerUDP
var server_broadcasting_udp_port: int = 6868
var _broadcast_timer = 0

# Our WebSocketClient instance
var _client = WebSocketClient.new()

func _connect():
	# Connect base signals to get notified of connection open, close, and errors.
	_client.connect("connection_closed", self, "_closed")
	_client.connect("connection_error", self, "_closed")
	_client.connect("connection_established", self, "_connected")
	_client.connect("data_received", self, "_on_data")

	# Initiate connection to the given URL.

	print("Attempting to connect to IP: ", state.ip, " ...")
	var err = _client.connect_to_url("ws://" + state.ip + ":9080")
#	var err = _client.connect_to_url("ws://echo.websocket.org")
	print(err)
	if err != OK:
		print("Unable to connect")
	state.connecting = false

func _closed(was_clean = false):
	print("Closed, clean: ", was_clean)
	state.connected = false
	state.connecting = false

func _connected(proto = ""):
	print("Connected with protocol: ", proto)
	_client.get_peer(1).put_packet("Test packet".to_utf8())
	state["connected"] = true

func _on_data():
	var data = _client.get_peer(1).get_packet().get_string_from_utf8()
	data = JSON.parse(data)
	print("Got data from server: ", data)
	if typeof(data.result) != TYPE_NIL:
		data = data.result
		if data.crash:
			Input.vibrate_handheld()


func _ready():
	udp_network = PacketPeerUDP.new()
	print(udp_network)
	if udp_network.listen(server_broadcasting_udp_port) != OK:
		print("Error listening on port: ", server_broadcasting_udp_port)
	else:
		print("Listening on port: ", server_broadcasting_udp_port)

func _process(_delta):
	if state.connected:
		_client.get_peer(1).put_packet(JSON.print(command).to_utf8())
	_client.poll()

	if udp_network.get_available_packet_count() > 0 and not state.connected and not state.connecting:
		var array_bytes = udp_network.get_packet()
		var packet_string = array_bytes.get_string_from_ascii()
		state.ip = udp_network.get_packet_ip()
		_connect()
		state.connecting = true
		var _scene = get_tree().change_scene("res://Control.tscn")

