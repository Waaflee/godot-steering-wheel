extends MarginContainer

var gravity: Vector3
var gravity_offset := 3.5
var max_gravity := 9.0

func _process(_delta):
	State.command["up"] = $HBoxContainer/VBoxContainer/CenterContainer/Button.pressed
	State.command["down"] = $HBoxContainer/VBoxContainer2/CenterContainer/Button.pressed
	get_rotation()
	
func get_rotation():
	gravity = Input.get_gravity()
	State.command["left"] = gravity.x < -gravity_offset
	State.command.left_degree = rotation_degree(gravity.x) if State.command.left else 0
	State.command["right"] = gravity.x > gravity_offset
	State.command.right_degree = rotation_degree(gravity.x) if State.command.right else 0
	
#func _physics_process(_delta):
#	print(Input.get_gravity())
#	print(State.command)

func rotation_degree(_gravity: float) -> float:
	var g := stepify(max(0, abs(_gravity) - gravity_offset), 0.01)
	return g/(max_gravity - gravity_offset)
